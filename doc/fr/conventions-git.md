# Conventions git du projet Durs

## Nommage des branches

### Branche créée par gitlab

Le plus souvent, votre branche sera nommée automatiquement par Gitlab puisque vous êtes censé créer votre branche en cliquant sur le bouton "Create a merge request" sur de l'issue liée.
Dans ce cas vous devez préfixer la branche par votre peseudo gitlab suivi d'un slash, exemple :

    elois/2-test-de-ticket

### Branche créée manuellement

Dans tout les autres cas, votre branche doit impérativement commencée par le pseudo de votre compte gitlab afin que tout un chacun sache qui travaille sur cette branche. Voici la convention a respecter pour les branches que vous créez manuellement :

    pseudo/type/description

pseudo := pseudo de votre compte gitlab.
type := voir "Liste des types de commit".
description := courte description en anglais a l'impératif présent, 3 à 4 mots maximum, pas d'articles.

Exemple :

    elois/ref/rename_module_trait

## Nommage des commit

Chaque commit doit suivre la convention suivante :

    [type] crate: action subject

Le type doit être un mot clé de type parmi la liste des types de commit.

La crate dolit être le nom de la crate concernée par le commit sans le préfixe `durs-`.

L'action doit être un verbe a l'impératif et le sujet un nom.

Exemple, renomage d'un trait `Toto` en `Titi` dans la crate `durs-bidule` :

    [ref] bidule: rename Toto -> Titi

### Liste des types de commit

* `style` : Modification du style du code (fmt et clippy).
* `feat` : Développement d'une nouvelle fonctionnalitée.
* `ref` : Refactoring de code.
* `fix` : Correction d'un bug
* `doc` : Modification de la documentation (y compris traduction et création de nouveau contenu).
* `ci` : Modification de la chaine d'intégration continue.
* `build` : Modification des script de build, de packaging ou/et de publication des livrables.
* `pub` : commit lié a la publication d'une crate sur [crates.io](https://crates.io).

Si vous avez besoin d'effectuer une action qui ne rentre dans aucun de ses types, contactez les principaux ndéveloppeurs du proejt pour discuter de l'ajout d'un nouveau type de commit dans cette liste.

## Stratégie de mise a jour

On met à jour uniquement avec des rebase, les merge sont strictement interdits !

Chaque fois que la branche `dev` est mise a jours, vous devez rebaser chacun de vos branche de travail sur dev. Pour chaque branche :

1. Placez vous sur votre branche
2. Lancez un rebase sur dev

    git rebase dev

3. Réglez les conflits s'il y en a. Une fois les conflits résolus vous devez :
    a. commiter les fichiers qui étaient en conflit
    b. Continuer le rebase avec la commande `git rebase --continue`
    c. Refaire 3. pour chaque commit ou il y a des conflits

4. Vous n'avez plus de conflits apmrès un `git rebase --continue`, c'est que le rebase est terminé. Passez a la branche suivante.

Si quelque chose s'est mal passé et que vous ne savez plus ou vous en êtes, vopus pouvez annuler votre rebase et reprendre de zéro avec la commande `git rebase --abort`.

Il se peut que vous n'ayez pas de conflits du tout, dans ce cas vous sautez directement de l'étape 2. à 4. sans passer par 3.

## Quand pusher

Idéalement a chaque fois que vous êtes sur le point d'etteindre votre ordinateur, soit environ 1 fois par jour (uniquement pour les jours ouv ous codez sur le proejt bien sûr).

Pensez bien a préfixer votre commit par `wip:` pour indiquer que c'est un "work in progress".

> Pourquoi puhser a lors que je n'ai pas fini ?

Si votre ordinateur rencontre un problème (panne, perte de données, reformatage, etc), pusher vous permet de vous assurez d'avoir toujours une copie de votre travail quelque part sur les internets.

## Comment merger ma contribution

Lorsque vous avez fini votre développement, éxécutez `fmt` et `clippy` pour être sur que votre code est propre puis éxécutez tout les tests pour être sur qu'ils passent :

    cargo +nightly fmt
    cargo +nightly clippy
    cargo test --all

Puis commitez le tout, sans le préfix wip- cette fois ci.

Ensuite néttoyez l'historique de votre branche avec un rebase interactif :

    git rebase -i dev

Renommez nottament les commit `wip:` et fusionnez les commits liés a fmt ou clippy afin de simplifier l'historique.

Enfin faite un push force sur le dépot distant :

    git push -f

Puis rendez vous sur le gitlab et vérifiez que le code sur votre branche distante est bien celui cencé s'y trouver.

Attendez 20 minutes que la chaien d'intégration continue puisse vérifier votre code, et si elle réussi vous pouvez alors supprimer la mention WIP de votre Merge Request et tagger des développeurs expérimentés pour demander une review.
